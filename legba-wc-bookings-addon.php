<?php
/**
 * Plugin Name:     Legba WC Bookings add-ons
 * Plugin URI:      https://www.id5.com.br
 * Description:     Add new capabilities to the WC Bookings plugin
 * Author:          ID5
 * Author URI:      https://www.id5.com.br
 * Text Domain:     legba-wc-bookings-addon
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Legba_Wc_Bookings_Addon
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'LEGBA_WC_BOOKINGS_ABSPATH' ) ) {
	define( 'LEGBA_WC_BOOKINGS_ABSPATH', dirname( __FILE__ ) . '/' );
}

if ( ! defined( 'LEGBA_WC_BOOKINGS_DIR_URL' ) ) {
	define( 'LEGBA_WC_BOOKINGS_DIR_URL', plugin_dir_url(__FILE__) );
}


require_once LEGBA_WC_BOOKINGS_ABSPATH . 'inc/functions/rewrite.php';
require_once LEGBA_WC_BOOKINGS_ABSPATH . 'inc/functions/settings.php';
require_once LEGBA_WC_BOOKINGS_ABSPATH . 'inc/functions/helpers.php';
require_once LEGBA_WC_BOOKINGS_ABSPATH . 'inc/functions/hooks.php';
require_once LEGBA_WC_BOOKINGS_ABSPATH . 'inc/functions/acf-form-group.php';
require_once LEGBA_WC_BOOKINGS_ABSPATH . 'inc/functions/booking-details.php';
require_once LEGBA_WC_BOOKINGS_ABSPATH . 'inc/functions/acf-form-translations.php';
require_once LEGBA_WC_BOOKINGS_ABSPATH . 'inc/functions/masks.php';

/**
 * Option key name for triggering activation notices.
 *
 * @since 1.14.4
 */
if ( ! defined( 'LEGBA_WC_BOOKINGS_ACTIVATION_NOTICE_KEY' ) ) {
	define( 'LEGBA_WC_BOOKINGS_ACTIVATION_NOTICE_KEY', 'legba_wc_bookings_show_activation_notice' );
}

/**
 * WooCommerce fallback notice.
 *
 * @since 1.13.0
 */
function legba_wc_bookings_missing_wc_notice() {
	/* translators: %s WC download URL link. */
	echo '<div class="error"><p><strong>' . sprintf( esc_html__( 'Legba WC Bookings addon requires WooCommerce to be installed and active. You can download %s here.', 'legba-wc-bookings-addon' ), '<a href="https://woocommerce.com/" target="_blank">WooCommerce</a>' ) . '</strong></p></div>';
}

function legba_wc_bookings_missing_wc_bookings_notice() {
	/* translators: %s WC download URL link. */
	echo '<div class="error"><p><strong>' . sprintf( esc_html__( 'Legba WC Bookings addon requires WooCommerce Bookings to be installed and active.', 'legba-wc-bookings-addon' )) . '</strong></p></div>';
}

register_activation_hook( __FILE__, 'legba_wc_bookings_activate' );

/**
 * Activation hook.
 */
function legba_wc_bookings_activate() {
	if ( ! class_exists( 'WooCommerce' ) ) {
		add_action( 'admin_notices', 'legba_wc_bookings_missing_wc_notice' );
		return;
	}
	
    if ( ! class_exists( 'WC_Bookings' ) ) {
		add_action( 'admin_notices', 'legba_wc_bookings_missing_wc_bookings_notice' );
		return;
	}    

	// Flag to trigger activation notice after includes have been loaded.
	update_option( LEGBA_WC_BOOKINGS_ACTIVATION_NOTICE_KEY, true );
}

function legba_wc_bookings_load_plugin() {

    if ( is_admin() && get_option( LEGBA_WC_BOOKINGS_ACTIVATION_NOTICE_KEY ) == true ) {
        delete_option( LEGBA_WC_BOOKINGS_ACTIVATION_NOTICE_KEY );

        update_option( 'booking_details_url', 'detalhes-da-reserva' );
        update_option( 'booking_details_url_en', 'detalhes-da-reserva-en' );
    }
}

add_action( 'admin_init', 'legba_wc_bookings_load_plugin' );
