<?php
/**
 * The main template file.
 *
 */

defined( 'ABSPATH' ) || exit( 'Direct script access denied.' );

$settings = [
    'post_id'       => 'legba_wc_bookings_new_post',
    'field_groups' => ['group_615cd1d2de9eb'],
    'new_post'      => array(
        'post_type'     => 'legba_booking_details',
        'post_status'   => 'publish'
    ),
    'submit_value'  => 'Reservar'    
];
$period = legba_wc_bookings_get_booking_period();

$url_redirect = '/produto/agendamento/';
$booking_type = legba_wc_bookings_get_booking_type();

if ($booking_type != false) {
    if ($booking_type == 'Estrangeiro') {
        $url_redirect = '/produto/agendamento/?wmc-currency=USD&amp;tipo=estrangeiro';
    }elseif ($booking_type == 'Brasileiro') {
        $url_redirect = '/produto/agendamento/?wmc-currency=BRL&amp;tipo=brasileiro';
    }
}



get_header();
?>
<?php


$l = etheme_page_config();

$content_layout = etheme_get_option('blog_layout', 'default');
$navigation_type = etheme_get_option( 'blog_navigation_type', 'pagination' );

$full_width = false;

if($content_layout == 'grid') {
	$full_width = etheme_get_option('blog_full_width', 0);
	$content_layout = 'grid';
}

if ( $content_layout == 'grid2' ) {
	$full_width = etheme_get_option('blog_full_width', 0);
	$content_layout = 'grid-2';
}

$class = 'hfeed';

$class .= ' et_blog-ajax';

$banner_pos = etheme_get_option( 'blog_page_banner_pos', 1 );

if ( $content_layout == 'grid' || $content_layout == 'grid-2' ) {
	$class .= ' row';
	if ( etheme_get_option( 'blog_masonry', 1 ) ) $class .= ' blog-masonry';
}

?>

<?php do_action( 'etheme_page_heading' ); ?>
	<div class="content-page <?php echo ( ! $full_width ) ? 'container' : 'blog-full-width'; ?> sidebar-mobile-<?php etheme_option('blog_sidebar_for_mobile'); ?>">
		<?php if ( $banner_pos == 3 ) { 
			if ( is_category() && category_description() ) : ?>
				<div class="blog-category-description"><?php echo do_shortcode( category_description() ); ?></div>
			<?php else:
				etheme_blog_header();
			endif; 
		} ?>
		<div class="sidebar-position-<?php echo esc_attr( $l['sidebar'] ); ?>">
			<div class="row">
				<div class="content <?php echo esc_attr( $l['content-class'] ); ?>">
					<?php 
					if( $banner_pos == 1 ) {
						if ( is_category() && category_description() ) : ?>
							<div class="blog-category-description"><?php echo do_shortcode( category_description() ); ?></div>
						<?php else:
							etheme_blog_header();
						endif;
					} ?>

                    <div class="<?php echo esc_attr($class); ?>">
                        <div class="detalhes-reserva-topo">
                            <h3>Dados dos visitantes</h3>
                            <h5>Início da reserva: <?php echo $period ? $period['date'] : '' ?>. Duração: <?php echo $period['period'] ? : '' ?>.</h3>
                            <a class="wc-bookings-alterar-periodo" href="<?php echo $url_redirect?>"><button class=" button button-primary button-large">Alterar período</button></a>
                        </div>
                    <?php
                    acf_form_head();
                    acf_form($settings);
                    ?>
                    </div>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
?>
