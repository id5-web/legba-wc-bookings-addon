<?php
var_dump(get_locale());
acf_form_head();

$settings = [
    'post_id'       => 'legba_wc_bookings_new_post',
    'field_groups' => ['group_615cd1d2de9eb'],
    'new_post'      => array(
        'post_type'     => 'legba_booking_details',
        'post_status'   => 'publish'
    ),
    'submit_value'  => 'Reservar'    
];
$period = legba_wc_bookings_get_booking_period();

$url_redirect = '/produto/agendamento/';
$booking_type = legba_wc_bookings_get_booking_type();

if ($booking_type != false) {
    if ($booking_type == 'Estrangeiro') {
        $url_redirect = '/produto/agendamento/?wmc-currency=USD&amp;tipo=estrangeiro';
    }elseif ($booking_type == 'Brasileiro') {
        $url_redirect = '/produto/agendamento/?wmc-currency=BRL&amp;tipo=brasileiro';
    }
}
?>
<div class="detalhes-reserva-topo">
    <h3>Dados dos usuários</h3>
    <h5>Início da reserva: <?php echo $period ? $period['date'] : '' ?>. Duração: <?php echo $period['period'] ? : '' ?>.</h3>
    <a class="wc-bookings-alterar-periodo" href="<?php echo $url_redirect?>"><button>Alterar período</button></a>
</div>
<?php
acf_form($settings);