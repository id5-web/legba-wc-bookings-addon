<?php
function legba_wc_bookings_scripts() {
    wp_enqueue_script( 'legba-wc-bookings-frontend', LEGBA_WC_BOOKINGS_DIR_URL . 'assets/js/frontend.js', ['jquery'], '1.0.0', true );
    wp_enqueue_style('legba-wc-bookings-frontend', LEGBA_WC_BOOKINGS_DIR_URL . 'assets/css/style.css', array(), filemtime(LEGBA_WC_BOOKINGS_ABSPATH . 'assets/css/style.css'), false);
}
add_action( 'wp_enqueue_scripts', 'legba_wc_bookings_scripts' );

function legba_wc_bookings_scripts_admin($hook) {
    if( 'post.php' != $hook )
    return;
    wp_register_style( 'legba-wc-bookings-backend', LEGBA_WC_BOOKINGS_DIR_URL . 'assets/css/style-admin.css', array(), filemtime(LEGBA_WC_BOOKINGS_ABSPATH . 'assets/css/style-admin.css'), false);
    wp_enqueue_style('legba-wc-bookings-backend');
}
add_action( 'admin_enqueue_scripts', 'legba_wc_bookings_scripts_admin' );

function legba_wc_bookings_init() {
	load_plugin_textdomain( 'legba-wc-bookings-addon', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );

	if ( ! class_exists( 'WooCommerce' ) ) {
		add_action( 'admin_notices', 'legba_wc_bookings_missing_wc_notice' );
		return;
	}

    if ( ! class_exists( 'WC_Bookings' ) ) {
		add_action( 'admin_notices', 'legba_wc_bookings_missing_wc_bookings_notice' );
		return;
	}    
}
add_action( 'plugins_loaded', 'legba_wc_bookings_init', 10 );

add_filter('booking_form_fields', 'legba_wc_bookings_set_default_resource', 10, 1);

function legba_wc_bookings_set_default_resource($fields)
{
    if ('en_US' == get_locale()) {
        if (!empty($_GET['tipo']) && $_GET['tipo'] == 'estrangeiro') {
            unset($fields['wc_bookings_field_resource']['options']['2227']);
        }
        
        if (!empty($_GET['tipo']) && $_GET['tipo'] == 'brasileiro') {
            unset($fields['wc_bookings_field_resource']['options']['2226']);
        }    
    } else {
        if (!empty($_GET['tipo']) && $_GET['tipo'] == 'estrangeiro') {
            unset($fields['wc_bookings_field_resource']['options']['2111']);
        }
        
        if (!empty($_GET['tipo']) && $_GET['tipo'] == 'brasileiro') {
            unset($fields['wc_bookings_field_resource']['options']['2112']);
        }        
    }
    
    return $fields;
}

function legba_wc_bookings_clear_cart()
{
    foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        WC()->cart->remove_cart_item( $cart_item_key );
    }
    return true;
}
add_action( 'woocommerce_before_single_product', 'legba_wc_bookings_clear_cart', 10 );

function legba_wc_bookings_add_to_cart_redirect( $url ) {
    if ('en_US' == get_locale()) {
        $booking_details_url = get_option('booking_details_url_en');    
    } else {
        $booking_details_url = get_option('booking_details_url');
    }
    return '/'.$booking_details_url;
}
add_filter( 'woocommerce_add_to_cart_redirect', 'legba_wc_bookings_add_to_cart_redirect', 10, 1 );

function legba_wc_bookings_maybe_hide_price($price){
    $clear = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($price))))));
    if($clear == "0 00"){
      return '';
    }
    return $price;
 } 

add_filter( 'woocommerce_get_price_html','legba_wc_bookings_maybe_hide_price');
add_filter( 'woocommerce_cart_item_price','legba_wc_bookings_maybe_hide_price');

add_filter( 'wc_add_to_cart_message_html', '__return_false' );

function legba_wc_bookings_checkout()
{
    var_dump(WC()->session->get( 'legba_wc_bookings_persons'));
}
//add_action('woocommerce_before_checkout_form', 'legba_wc_bookings_checkout');

function legba_wc_bookings_cart_item_removed( $cart_item_key, $instance ) { 
    legba_wc_bookings_clear_cart();    
    WC()->session->__unset( 'legba_wc_bookings_persons' );
}; 
add_action( 'woocommerce_cart_item_removed', 'legba_wc_bookings_cart_item_removed', 10, 2 ); 

function legba_wc_bookings_cart_return_booking_details()
{
    $booking_details_url = get_option('booking_details_url');
    if ('en_US' == get_locale()) {
?>
        <a class="wc-bookings-retorn-booking-details" href="#" onclick="window.history.go(-1)">Change Booking </a>
        <span class="clearfix"></span>        
<?php
    } else {
?>
        <a class="wc-bookings-retorn-booking-details" href="#" onclick="window.history.go(-1)">Alterar reserva</a>
        <span class="clearfix"></span>
<?php
    }
}

add_action( 'woocommerce_review_order_before_payment', 'legba_wc_bookings_cart_return_booking_details', 10, 2 ); 


function legba_wc_bookings_auto_create_booking( $booking_id ) {
	
	// Get the previous booking from the ID
	$prev_booking = get_wc_booking( $booking_id );
    $product = get_page_by_title( "Ingressos", OBJECT, 'product' );
	
    $legba_wc_booking_persons = WC()->session->get( 'legba_wc_bookings_persons');
    foreach ($legba_wc_booking_persons['persons'] as $person) {
        // Don't want follow ups for follow ups
        if ( $prev_booking->get_parent_id() <= 0 ) {
            // Set the follow up data
            $new_user = false;
            $user = get_user_by('email', $person['email']);
            if ($user != false) {
                $user_id = $user->ID;    
            } else {
                $new_user = true;
                $user_id = wc_create_new_customer( $person['email'], $person['email'], '@fN7rH9oK8zN5jW0l' );        
            }

            if ($new_user) {
                wp_update_user([
                    'ID' => $user_id, 
                    'first_name' => $person['nome']
                ]);
            }

            $new_booking_data = [
                'start_date'  => $prev_booking->get_start(), // same time, 1 week on
                'end_date'    => $prev_booking->get_end(),   // same time, 1 week on
                //'parent_id'   => $booking_id,    
               // 'order_id'    => $prev_booking->get_order_id(),
                'user_id'     => $user_id,                                    // set the parent
                'person_counts' => [1],
            ];

            $new_booking = create_wc_booking( 
                $product->ID, 
                $new_booking_data,               // Use the data pulled above
                $prev_booking->get_status(),     // Match previous bookings status
                true                            // Not exact, look for a slot
            );
            
            update_post_meta($new_booking->get_id(), 'nome', $person['nome']);
            update_post_meta($new_booking->get_id(), 'email', $person['email']);
            update_post_meta($new_booking->get_id(), 'tipo_experiencia', $person['tipo_experiencia']);
            update_post_meta($new_booking->get_id(), 'nacionalidade', $person['nacionalidade']);
            
            if ($person['tipo_experiencia'] == 'Trilha') {
                update_post_meta($new_booking->get_id(), 'guia', $legba_wc_booking_persons['guia']);
            }

            if (array_key_exists('passaporte', $person)) {
                update_post_meta($new_booking->get_id(), 'passaporte', $person['passaporte']);
            }
            if (array_key_exists('cpf', $person)) {
                update_user_meta($user_id, 'billing_cpf', $person['cpf']);

                update_post_meta($new_booking->get_id(), 'cpf', $person['cpf']);
            }

            if($person['nacionalidade'] == 'Brasileiro') {
                update_post_meta($new_booking->get_id(), 'estado', $person['estado']);
            } else {
                update_post_meta($new_booking->get_id(), 'estado_estrangeiro', $person['estado_estrangeiro']);
            }
        }
    }
}
add_action( 'woocommerce_booking_in-cart_to_paid', 'legba_wc_bookings_auto_create_booking' );
add_action( 'woocommerce_booking_in-cart_to_unpaid', 'legba_wc_bookings_auto_create_booking' );
//add_action( 'woocommerce_booking_unpaid_to_paid', 'legba_wc_bookings_auto_create_booking' );
//add_action( 'woocommerce_booking_confirmed_to_paid', 'legba_wc_bookings_auto_create_booking' );

###################################################
//Set username to user email as default
###################################################
function legba_wc_bookings_new_customer_username( $username, $email, $new_user_args, $suffix ) {
    return $email;
}
add_filter( 'woocommerce_new_customer_username', 'legba_wc_bookings_new_customer_username', 10, 4 );

###################################################
//Allow custom characters inside of wordpress usernames
###################################################
function legba_wc_bookings_sanitize_user($username, $raw_username, $strict) {	
	$allowed_symbols = "a-z0-9+ _.\-@"; //yes we allow whitespace which will be trimmed further down script
	
	//Strip HTML Tags
	$username = wp_strip_all_tags ($raw_username);

	//Remove Accents
	$username = remove_accents ($username);

	//Kill octets
	$username = preg_replace ('|%([a-fA-F0-9][a-fA-F0-9])|', '', $username);

	//Kill entities
	$username = preg_replace ('/&.+?;/', '', $username);

	//allow + symbol
	$username = preg_replace ('|[^'.$allowed_symbols.']|iu', '', $username);

	//Remove Whitespaces
	$username = trim ($username);

	// Consolidate contiguous Whitespaces
	$username = preg_replace ('|\s+|', ' ', $username);

	return $username;
}

add_filter ('sanitize_user', 'legba_wc_bookings_sanitize_user', 10, 3);

function legba_wc_bookings_register_meta_boxes()
{
    add_meta_box(
        'legba-wc-bookings-addon',
        __( 'Detalhes da reserva', 'legba-wc-bookings-addon' ),
        'legba_wc_booking_meta_box_inner',
        'wc_booking',
        'side',
        'low'
    );    
}


add_action( 'add_meta_boxes', 'legba_wc_bookings_register_meta_boxes' );

function legba_wc_booking_meta_box_inner()
{
    global $post;
//    global $booking;

//    if ( ! is_a( $booking, 'WC_Booking' ) || $booking->get_id() !== $post->ID ) {
        $booking = new WC_Booking( $post->ID );
//    }
    $has_data = false;
    ?>
    <table class="booking-customer-details">
    <?php
    $booking_customer_id = $booking->get_customer_id();
    $user                = $booking_customer_id ? get_user_by( 'id', $booking_customer_id ) : false;

    if ( $booking_customer_id && $user ) {
        $customer = $booking->get_customer();
        ?>
        <tr>
            <th><?php esc_html_e( 'Name:', 'woocommerce-bookings' ); ?></th>
            <td><?php echo esc_html( $user->first_name ? $user->first_name : '&mdash;' ); ?></td>
        </tr>
        <tr>
            <th><?php esc_html_e( 'Email:', 'woocommerce-bookings' ); ?></th>
            <td><?php echo wp_kses_post( make_clickable( sanitize_email( $user->user_email ) ) ); ?></td>
        </tr>
        <tr>
            <th><?php esc_html_e( 'Experiência:', 'legba-wc-bookings-addon' ); ?></th>
            <td><?php echo wp_kses_post( get_post_meta($booking->get_id(), 'tipo_experiencia', true )); ?></td>
        </tr>        
        <tr>
            <th><?php esc_html_e( 'Nacionalidade:', 'legba-wc-bookings-addon' ); ?></th>
            <td><?php echo wp_kses_post( get_post_meta($booking->get_id(), 'nacionalidade', true )); ?></td>
        </tr>                
        <?php if ($cpf = get_post_meta($booking->get_id(), 'cpf', true )) : ?>                
        <tr>
            <th><?php esc_html_e( 'CPF:', 'legba-wc-bookings-addon' ); ?></th>
            <td><?php echo wp_kses_post($cpf); ?></td>
        </tr>                            
        <?php elseif ($passaporte = get_post_meta($booking->get_id(), 'passaporte', true )) : ?>
        <tr>    
            <th><?php esc_html_e( 'Passaporte:', 'legba-wc-bookings-addon' ); ?></th>
            <td><?php echo wp_kses_post($passaporte); ?></td>
        </tr>
        <?php endif;?>
        <?php if(get_post_meta($booking->get_id(), 'nacionalidade', true ) == 'Brasileiro') :?>
            <tr>
                <th><?php esc_html_e( 'Estado:', 'legba-wc-bookings-addon' ); ?></th>
                <td><?php echo wp_kses_post(get_post_meta($booking->get_id(), 'estado', true )); ?></td>
            </tr>                                    
        <?php else :?>
            <tr>
                <th><?php esc_html_e( 'Estado:', 'legba-wc-bookings-addon' ); ?></th>
                <td><?php echo wp_kses_post(get_post_meta($booking->get_id(), 'estado_estrangeiro', true )); ?></td>
            </tr>                                    
        <?php endif; ?>
        <?php if ($guia = get_post_meta($booking->get_id(), 'guia', true )) : ?>                
        <tr>
            <th><?php esc_html_e( 'Guia:', 'legba-wc-bookings-addon' ); ?></th>
            <td><?php echo wp_kses_post($guia); ?></td>
        </tr>         
        <?php endif;               
    }

    ?>
    </table>
    <?php
}

/*
	EM ESTUDO
add_filter( 'woocommerce_add_cart_item_data', 'save_custom_product_field_in_cart', 10, 2 );
function save_custom_product_field_in_cart( $cart_item_data, $product_id ) {
    if( isset( $_POST['idp'] ) )
        $cart_item_data[ 'idp' ] = sanitize_text_field($_POST['idp']);

    return $cart_item_data;
}
*/