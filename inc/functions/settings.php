<?php
// create custom plugin settings menu
add_action('admin_menu', 'legba_wc_bookings_settings');

function legba_wc_bookings_settings() {

	//create new top-level menu
	add_submenu_page('options-general.php', 'Legba WC Bookings addon', 'Legba WC Bookings addon', 'administrator', __FILE__, 'legba_wc_bookings_settings_page' );

	//call register settings function
	add_action( 'admin_init', 'register_legba_wc_bookings_settings' );
}


function register_legba_wc_bookings_settings() {
	//register our settings
	register_setting( 'legba-wc-bookings-settings-group', 'booking_details_url' );
    register_setting( 'legba-wc-bookings-settings-group', 'booking_details_url_en' );
    register_setting( 'legba-wc-bookings-settings-group', 'booking_details_guias' );
}

function legba_wc_bookings_settings_page() {
?>
    <div class="wrap">
    <h1>Legba WC Bookings addon</h1>

    <form method="post" action="options.php">
        <?php settings_fields( 'legba-wc-bookings-settings-group' ); ?>
        <?php do_settings_sections( 'legba-wc-bookings-settings-group' ); ?>
        <table class="form-table">
            <tr valign="top">
            <th scope="row">URL da página de detalhes do agendamento (padrão para detalhes-da-reserva)</th>
            <td><input type="text" name="booking_details_url" value="<?php echo esc_attr( get_option('booking_details_url') ); ?>" /></td>
            </tr>         
            <tr valign="top">
            <th scope="row">URL da página de detalhes do agendamento em inglês(padrão para detalhes-da-reserva-en)</th>
            <td><input type="text" name="booking_details_url_en" value="<?php echo esc_attr( get_option('booking_details_url_en') ); ?>" /></td>
            </tr>         
            <tr valign="top">
            <th scope="row">Guias (Um por linha)</th>
            <td><textarea cols="30" rows="30" name="booking_details_guias"><?php echo esc_attr( get_option('booking_details_guias') ); ?></textarea>
            </tr>                     
        </table>
        
        <?php submit_button(); ?>

    </form>
    </div>
<?php 
}

//add_action('update_option_booking_details_url', 'legba_wc_bookings_settings_configure', 10);
//add_action('update_option_booking_details_url_en', 'legba_wc_bookings_settings_configure', 10);