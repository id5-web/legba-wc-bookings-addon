<?php
function legba_wc_bookings_get_persons_quantity()
{
    $persons_quantity = 0;
    $booking = null;
    if (is_admin()) return 0;
    foreach(WC()->cart->get_cart() as $cart_item) {
        // The item persons count
        if (array_key_exists('booking', $cart_item)) {
            $persons_quantity = array_sum( $cart_item['booking']['_persons'] );
    
            // Get the instance of the WC_Product_Booking product
            //$booking = new WC_Product_Booking( $cart_item['product_id'] );    
        }  
    }
    return $persons_quantity;
}

function legba_wc_bookings_get_booking_type()
{
    $booking_type = false;
    foreach(WC()->cart->get_cart() as $cart_item) {
        // The item persons count
        if (array_key_exists('booking', $cart_item)) {
            $booking_type = $cart_item['booking']['type'];
    
            if ('en_US' == get_locale()) {
                if ($booking_type == 'Foreign') {
                    return 'Estrangeiro';
                }
                if ($booking_type == 'Brazilian') {
                    return 'Brasileiro';
                }                
            }
            // Get the instance of the WC_Product_Booking product
            //$booking = new WC_Product_Booking( $cart_item['product_id'] );    
        }  
    }
    return $booking_type;
}

function legba_wc_bookings_get_booking_period()
{
    $booking_type = false;
    foreach(WC()->cart->get_cart() as $cart_item) {
        // The item persons count
        if (array_key_exists('booking', $cart_item)) {
            $data = $cart_item['booking'];
            return [
                'date' => $data['_day'].'/'.$data['_month'].'/'.$data['_year'],
                'period' => $data['duration'],
            ];
        }  
    }
    return false;
}
