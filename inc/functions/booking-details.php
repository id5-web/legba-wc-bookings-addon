<?php
function legba_wc_bookings_booking_details_form()
{
    $persons_quantity = legba_wc_bookings_get_persons_quantity();
    if ($persons_quantity == 0) {
        $legba_redirect_link = get_home_url();
        if ( wp_get_referer() )
        {
            $legba_redirect_link = wp_get_referer(); 
        }
        echo '<div class="legba-wc-booking-empty-cart">';
        if ('en_US' == get_locale()) {
            echo '<h4 class="legba-wc-booking-empty-cart-error">Your cart is empty!</h4>';
            echo '<a href="'.$legba_redirect_link.'"><button class="legba-wc-booking-empty-cart-booking">Go back</button>';    
        } else {
            echo '<h4 class="legba-wc-booking-empty-cart-error">Seu carrinho está vazio!</h4>';
            echo '<a href="'.$legba_redirect_link.'"><button class="legba-wc-booking-empty-cart-booking">Voltar</button>';    
        }     
        echo '</div>';
        return;
    }    

    require_once LEGBA_WC_BOOKINGS_ABSPATH . 'templates/booking-details.php';
}

//HOOKS PARA CONTROLE DO FLUXO DO FORMULÁRIO
function legba_wc_bookings_set_persons_quantity($field)
{
    // see all the field settings before changing
    //echo '<pre>'; print_r($field); echo '</pre>';
    $persons_quantity = legba_wc_bookings_get_persons_quantity();

    $field['max'] = $persons_quantity;
    $field['min'] = $persons_quantity;

    return $field;
}

add_filter('acf/prepare_field/key=field_615d8528bf772', 'legba_wc_bookings_set_persons_quantity');

function legba_wc_bookings_add_products_to_cart ($post_id) {
    // check if this is to be a new post
    //        if( $post_id != 'new' ) {
    //            return $post_id;    
    //        }

    if (
        !array_key_exists('_acf_post_id', $_POST) || 
        (array_key_exists('_acf_post_id', $_POST) && $_POST['_acf_post_id'] != 'legba_wc_bookings_new_post')
    ) {

        return $post_id;
    }
    
    $legba_wc_bookings_fields_mapping = legba_wc_bookings_get_fields_mapping();
    $acf_data = $_POST['acf'];
    unset($_POST['acf']);
    unset($_POST['return']);    

    

    $cart_data = WC()->cart->get_cart();
    if ( sizeof( $cart_data ) <= 0 ) {
        wp_redirect( get_home_url(), 301);
		exit;	        
    }
    
    //Em caso de reedição do formulário remove antes de adicionar as experiências novamente
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        if (!array_key_exists('booking', $cart_item)) {
            remove_action( 'woocommerce_cart_item_removed', 'legba_wc_bookings_cart_item_removed'); 
             WC()->cart->remove_cart_item( $cart_item_key );
             add_action( 'woocommerce_cart_item_removed', 'legba_wc_bookings_cart_item_removed', 10, 2 ); 
        }
    }    

    $legba_wc_booking_item_data = [];

    if (array_key_exists($legba_wc_bookings_fields_mapping['detalhes_da_reserva'], $acf_data)) {
        foreach ($acf_data[$legba_wc_bookings_fields_mapping['detalhes_da_reserva']] as $key => $person) {

            $legba_wc_booking_item_data['persons'][$key] = [
                'nome' => $person[$legba_wc_bookings_fields_mapping['nome']],
                'email' => $person[$legba_wc_bookings_fields_mapping['email']],
                'tipo_experiencia' => $person[$legba_wc_bookings_fields_mapping['tipo_de_experiencia']],
                'nacionalidade' => $person[$legba_wc_bookings_fields_mapping['nacionalidade']],
            ];

            $experiencia = $person[$legba_wc_bookings_fields_mapping['tipo_de_experiencia']];

            if ($person[$legba_wc_bookings_fields_mapping['nacionalidade']] == 'Brasileiro') {
                $legba_wc_booking_item_data['persons'][$key]['estado'] = $person[$legba_wc_bookings_fields_mapping['estado']];
            } else {
                $legba_wc_booking_item_data['persons'][$key]['estado_estrangeiro'] = $person[$legba_wc_bookings_fields_mapping['estado_estrangeiro']];
            }

            //Adiciona produtos ao carrinho
            if (array_key_exists($legba_wc_bookings_fields_mapping['cpf'], $person)) {
                $legba_wc_booking_item_data['persons'][$key]['cpf'] = $person[$legba_wc_bookings_fields_mapping['cpf']];

                $product = get_page_by_title( "{$experiencia} - Brasileiro", OBJECT, 'product' );
                $product_id = $product->ID;     
            }

            if (array_key_exists($legba_wc_bookings_fields_mapping['passaporte'], $person)) {
                $legba_wc_booking_item_data['persons'][$key]['passaporte'] = $person[$legba_wc_bookings_fields_mapping['passaporte']];

                $product = get_page_by_title( "{$experiencia} - Estrangeiro", OBJECT, 'product' );
                $product_id = $product->ID; 
            }

            WC()->cart->add_to_cart( $product_id);

            //WC()->cart->add_to_cart( $product_id, $quantity, NULL, NULL, $legba_wc_booking_item_data);
        }

        $legba_wc_booking_item_data['guia'] = $acf_data[$legba_wc_bookings_fields_mapping['guia']] ?? '';

        if ($legba_wc_booking_item_data['guia'] == 'outro') {
            $legba_wc_booking_item_data['guia'] = $acf_data[$legba_wc_bookings_fields_mapping['guia_outro']];
        }

        WC()->session->set('legba_wc_bookings_persons', $legba_wc_booking_item_data);

        $legba_wc_booking_checkout = wc_get_checkout_url();

        echo '<div class="legba-wc-booking-success">';
        if ('en_US' == get_locale()) {
            echo '<h3 class="legba-wc-booking-success-message">Registration completed successfully!</h3>';
            echo '<p class="legba-wc-booking-success-message">You will be taken to the booking completion page.</p>';                
        } else {
            echo '<h3 class="legba-wc-booking-success-message">Cadastro concluído com sucesso!</h3>';
            echo '<p class="legba-wc-booking-success-message">Você será direcionado para a página de conclusão da reserva.</p>';                
        }
        echo '</div>';
        echo '
        <script>
            window.location.href = "'.$legba_wc_booking_checkout.'";
        </script>';     
        die();
    }
}

add_filter('acf/pre_save_post' , 'legba_wc_bookings_add_products_to_cart', 10, 1 );    

function legba_wc_bookings_booking_default_tipo_de_documento($field) {
    $legba_wc_bookings_fields_mapping = legba_wc_bookings_get_fields_mapping();
    $booking_type = legba_wc_bookings_get_booking_type();

    if ($booking_type != false) {
        if ($booking_type == 'Estrangeiro') {
            $tipo_documento = 'Passaporte';            
        }elseif ($booking_type == 'Brasileiro') {
            $tipo_documento = 'CPF';            
        }

        if (!$tipo_documento) {
            return $field;
        }

        $field['default_value'] = [$tipo_documento];
        $field['disabled'] = true;
    }
    return $field;
}

$legba_wc_bookings_fields_mapping = legba_wc_bookings_get_fields_mapping();

add_filter(
    "acf/load_field/key={$legba_wc_bookings_fields_mapping['tipo_de_documento']}", 
    'legba_wc_bookings_booking_default_tipo_de_documento', 10, 1
);    

function legba_wc_bookings_booking_default_nacionalidade($field) {
    $legba_wc_bookings_fields_mapping = legba_wc_bookings_get_fields_mapping();
    $booking_type = legba_wc_bookings_get_booking_type();

    if ($booking_type != false) {
        if ($booking_type == 'Estrangeiro') {
            unset($field['choices']['Brasileiro']);
        }
    }
    return $field;
}

add_filter(
    "acf/load_field/key={$legba_wc_bookings_fields_mapping['nacionalidade']}", 
    'legba_wc_bookings_booking_default_nacionalidade', 10, 1
);    

function legba_wc_bookings_booking_guia_choices( $field ) {
    $curr_user_id = get_current_user_id();

    if (!empty($curr_user_id)) {
        $memberships = wc_memberships_get_user_memberships( $curr_user_id );

        if ( (is_array($memberships) && count($memberships) > 0) && $memberships[0]->plan->slug == 'guias' ) { 
            $field['choices'] = array();    
            $field['choices'][ 'outro' ] = 'Outro';
            $field['default_value'] = 'outro';
            $field['value'] = 'outro';
            $field['allow_null'] = 0;

            return $field;
        }
    }

    $field['choices'] = array();    
    $choices = get_option('booking_details_guias');
    $choices = explode("\n", $choices);    
    $choices = array_map('trim', $choices);
    
    if( is_array($choices) ) {
        foreach( $choices as $choice ) {
            $field['choices'][ $choice ] = $choice;
        }
        $field['choices'][ 'outro' ] = 'Outro';
    }
    return $field;
}

add_filter('acf/load_field/name=guia', 'legba_wc_bookings_booking_guia_choices');

function legba_wc_bookings_booking_guia_outro_default( $field ) {
    $curr_user = wp_get_current_user();

    if (!empty($curr_user)) {        
        //Se o usuário é membro do grupo de guias o guia é ele mesmo.
        $memberships = wc_memberships_get_user_memberships( $curr_user->ID );
        if ( is_array($memberships) && count($memberships) > 0 && $memberships[0]->plan->slug == 'guias' ) { 
            $field['value'] = "$curr_user->user_firstname $curr_user->user_lastname";
            return $field;
        }
    }

    return $field;
}


add_filter('acf/load_field/name=guia_outro', 'legba_wc_bookings_booking_guia_outro_default');

add_filter("acf/validate_value/key={$legba_wc_bookings_fields_mapping['guia']}", 'legba_wc_bookings_validate_guia', 10, 4);

function legba_wc_bookings_validate_guia( $valid, $value, $field, $input ){
    $legba_wc_bookings_fields_mapping = legba_wc_bookings_get_fields_mapping();

	// bail early if value is already invalid
	if( !$valid ) {return $valid; }

    $acf_data = $_POST['acf'];
    $trilha = false;
    if (array_key_exists($legba_wc_bookings_fields_mapping['detalhes_da_reserva'], $acf_data)) {
        foreach ($acf_data[$legba_wc_bookings_fields_mapping['detalhes_da_reserva']] as $key => $person) {
            if ($person[$legba_wc_bookings_fields_mapping['tipo_de_experiencia']] == 'Trilha') {
                $trilha = true;
            }
        } 
    } 

	if($trilha){
		if(empty($value)){
            if ('en_US' == get_locale()) {
                $valid = __('The guide is mandatory when booking a trail.');
            } else {
                $valid = __('O guia é obrigatório em caso de reserva com trilha.');
            }
		}
	}
    return $valid;	
}

add_shortcode( 'legba_wc_bookings_form', 'legba_wc_bookings_booking_details_form' );

function legba_wc_bookings_set_repeater( $value, $post_id, $field ){
    
    $legba_wc_bookings_fields_mapping = legba_wc_bookings_get_fields_mapping();
    echo '<pre>';
    var_dump($field, $legba_wc_bookings_fields_mapping);die();
    //    $value = array();
    
    // Nomes dos guias
    //$settings_values = get_field('medication_types','option');
    
//    foreach( $settings_values as $settings_value ){
//        $value []= array('field_1234567890abc' => $settings_value['name_field'], 'field_abc1234567890' => '');
//    }

}

//acf/load_value/name={$field_name} //- filter for a specific value load based on it's field name
//add_filter('acf/load_value/name=detalhes_da_reserva', 'legba_wc_bookings_set_repeater', 10, 3);        

