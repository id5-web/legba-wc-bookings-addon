<?php
function legba_wc_bookings_booking_acf_translations( $field ) {
    $locale = get_locale();

    if ($locale == 'en_US') {
        switch ($field['label']) {
            case 'Nome':
                $field['label'] = 'Name';
                break;
            case 'Tipo de Documento':
                $field['label'] = 'Document Type';
                break;            
            case 'Passaporte':
                $field['label'] = 'Passport';
                break;
            case 'Nacionalidade':
                $field['label'] = 'Nationality';
                break;
            case 'Estado':
                $field['label'] = 'State';
                break;                                
            case 'Tipo de experiência':
                $field['label'] = 'Type of Experience';
                $field['instructions'] = 'To make a trail through Lajedo it is necessary to have an authorized guide.';
                break;                                            
            case 'Guia contratado':
                $field['label'] = 'Hired guide';
                $field['instructions'] = 'If you have your own guide, please request registration <a href="/cadastro-de-guias">here</a> ';                
                break;                                                       
            case 'Guia':
                $field['label'] = 'Guide';
                break;                                                                    
        }    

        if (array_key_exists('choices', $field)) {
            switch ($field['name']) {
                case 'tipo_de_documento':
                    $field['choices'] = array(
                        'CPF' => 'CPF',
                        'Passaporte' => 'Passport',
                    );
                    break;
                case 'nacionalidade':
                    $field['choices'] = array(
                        'Antiguano' => 'Antiguan',
                        'Argentino' => 'Bahamian',
                        'Bahamense' => 'Barbadian',
                        'Barbadiano, barbadense' => 'Barbadian',
                        'Belizenho' => 'Belizean',
                        'Boliviano' => 'Boliviano',
                        'Brasileiro' => 'Brazilian',
                        'Chileno' => 'Chilean',
                        'Colombiano' => 'Colombian',
                        'Costarriquenho' => 'Costa Rican',
                        'Cubano' => 'Cuban',
                        'Dominicano' => 'Dominican',
                        'Equatoriano' => 'Ecuadorian',
                        'Salvadorenho' => 'Salvadorenho',
                        'Granadino' => 'Grenadine',
                        'Guatemalteco' => 'Guatemaltec',
                        'Guianês' => 'Guyanese',
                        'Guianense' => 'Guyanian',
                        'Haitiano' => 'Haitian',
                        'Hondurenho' => 'Honduran',
                        'Jamaicano' => 'Jamaican',
                        'Mexicano' => 'Mexican',
                        'Nicaraguense' => 'Nicaraguan',
                        'Panamenho' => 'Panamanian',
                        'Paraguaio' => 'Paraguay',
                        'Peruano' => 'Peruvian',
                        'Portorriquenho' => 'Puertorian',
                        'Dominicana' => 'Dominican',
                        'São-cristovense' => 'Saint-Christovense',
                        'São-vicentino' => 'Saint Vincentian',
                        'Santa-lucense' => 'Santa Lucense',
                        'Surinamês' => 'Surinam',
                        'Trindadense' => 'Trindadense',
                        'Uruguaio' => 'Uruguayan',
                        'Venezuelano' => 'Venezuelan',
                        'Alemão' => 'German',
                        'Austríaco' => 'Austrian',
                        'Belga' => 'Belgian',
                        'Croata' => 'Croatian',
                        'Dinamarquês' => 'Danish',
                        'Eslovaco' => 'Slovak',
                        'Esloveno' => 'Slovenian',
                        'Espanhol' => 'Spanish',
                        'Francês' => 'French',
                        'Grego' => 'Greek',
                        'Húngaro' => 'Hungarian',
                        'Irlandês' => 'Irish',
                        'Italiano' => 'Italian',
                        'Noruego' => 'Norway',
                        'Holandês' => 'Dutch',
                        'Polonês' => 'Polish',
                        'Português' => 'Portuguese',
                        'Britânico' => 'British',
                        'Inglês' => 'English',
                        'Galês' => 'Welsh',
                        'Escocês' => 'Scottish',
                        'Romeno' => 'Romanian',
                        'Russo' => 'Russian',
                        'Sérvio' => 'Serbian',
                        'Sueco' => 'Swedish',
                        'Suíço' => 'Swiss',
                        'Turco' => 'Turk',
                        'Ucraniano' => 'Ukrainian',
                        'Americano' => 'American',
                        'Canadense' => 'Canadian',
                        'Angolano' => 'Angolan',
                        'Moçambicano' => 'Mozambican',
                        'Sul-africano' => 'South African',
                        'Zimbabuense' => 'Zimbabwean',
                        'Argélia' => 'Algeria',
                        'Comorense' => 'Comoros',
                        'Egípcio' => 'Egyptian',
                        'Líbio' => 'Libyan',
                        'Marroquino' => 'Moroccan',
                        'Ganés' => 'Ganese',
                        'Queniano' => 'Kenyan',
                        'Ruandês' => 'Rwandan',
                        'Ugandense' => 'Ugandanian',
                        'Bechuano' => 'Bechuan',
                        'Marfinense' => 'Ivorian',
                        'Camaronense' => 'Cameroonian',
                        'Nigeriano' => 'Nigerian',
                        'Somali' => 'Somali',
                        'Australiano' => 'Australian',
                        'Neozelandês' => 'New Zealander',
                        'Afegão' => 'Afghan',
                        'Saudita' => 'Saudi',
                        'Armeno' => 'Armeno',
                        'Bangladesh' => 'Bangladesh',
                        'Chinês' => 'Chinese',
                        'Norte-coreano, coreano' => 'North Korean',
                        'Sul-coreano, coreano' => 'South Korean',
                        'Indiano' => 'Indian',
                        'Indonésio' => 'Indonesian',
                        'Iraquiano' => 'Iraqi',
                        'Iraniano' => 'Iranian',
                        'Israelita' => 'Israelite',
                        'Japonês' => 'Japanese',
                        'Malaio' => 'Malay',
                        'Nepalês' => 'Nepalese',
                        'Omanense' => 'Omani',
                        'Paquistanês' => 'Pakistani',
                        'Palestino' => 'Palestinian',
                        'Qatarense' => 'Qataris',
                        'Sírio' => 'Syrian',
                        'Cingalês' => 'Sinhala',
                        'Tailandês' => 'Thai',
                        'Timorense, maubere' => 'Timorese, maubere',
                        'Árabe, emiratense' => 'Arab, Emirati',
                        'Vietnamita' => 'Vietnamese',
                        'Iemenita' => 'Yemenite',
                    );
                    
                    $booking_type = legba_wc_bookings_get_booking_type();

                    if ($booking_type != false) {
                        if ($booking_type == 'Estrangeiro') {
                            unset($field['choices']['Brasileiro']);
                        }
                    }

                    break;
    
                case 'tipo_de_experiencia':
                    $field['choices'] = array(
                        'Jardins' => 'Gardens',
                        'Trilha' => 'Gardens + Trail',
                    );
                    break;          
                case 'aceite':
                    $field['choices'] = array(
                        'Estou ciente' => 'I am aware that Lajedo dos Beija-flores is in an open environment, exposed to the interference of nature, and that the entry of minors will only be allowed for the exclusive purpose of observation and nature photography, provided they are accompanied by a responsible adult.'
                    );
                    break;                              
            }    
        }        
    }

    return $field;
}

add_filter('acf/load_field', 'legba_wc_bookings_booking_acf_translations');