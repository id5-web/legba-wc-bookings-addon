<?php
function legba_wc_bookings_settings_configure() {
    $booking_details_url = get_option('booking_details_url');
    $booking_details_url_en = get_option('booking_details_url_en');
    legba_wc_bookings_init_rewrite($booking_details_url, $booking_details_url_en);
}

add_action('init', 'legba_wc_bookings_settings_configure', 10);

//add_action( 'init', 'legba_wc_bookings_init' );
function legba_wc_bookings_init_rewrite($booking_details_url, $booking_details_url_en)
{
    if (!empty($booking_details_url)) {
        add_rewrite_rule( $booking_details_url, 'index.php?legba_booking_details=1', 'top' );
        // Flush Permalinks.
    }
    if (!empty($booking_details_url_en)) {
        add_rewrite_rule( $booking_details_url_en, 'index.php?legba_booking_details=2', 'top' );
        // Flush Permalinks.
    }    
    flush_rewrite_rules();
}

add_filter( 'query_vars', 'legba_wc_bookings_query_vars' );
function legba_wc_bookings_query_vars( $query_vars )
{
    $query_vars[] = 'legba_booking_details';
    return $query_vars;
}

add_action( 'parse_request', 'legba_wc_bookings_parse_request' );
function legba_wc_bookings_parse_request( &$wp )
{
    if ( array_key_exists( 'legba_booking_details', $wp->query_vars ) ) {
        if ($wp->query_vars['legba_booking_details'] == '1') {
            require_once LEGBA_WC_BOOKINGS_ABSPATH . 'templates/booking-details.php';
        } elseif ($wp->query_vars['legba_booking_details'] == '2') {
            require_once LEGBA_WC_BOOKINGS_ABSPATH . 'templates/en-booking-details.php';
        }
        exit();
    }
    return;
}